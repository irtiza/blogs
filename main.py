from fastapi import FastAPI
from typing import Optional
from pydantic import BaseModel


app = FastAPI()


@app.get('/')
def index():
    return {'data': 'blogs apis'}


